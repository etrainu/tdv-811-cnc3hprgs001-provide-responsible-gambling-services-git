﻿var scrollHeight:Number = scrollTrack.height;
var contentHeight:Number = contentMain.height;
var scrollFaceHeight:Number = scrollFace.height;
var maskHeight:Number = maskedView.height;
var initPosition:Number = scrollFace.y = scrollTrack.y;
var initContentPos:Number = contentMain.y;
var finalContentPos:Number = maskHeight-contentHeight+initContentPos;
var left:Number = scrollTrack.x;
var top:Number = scrollTrack.y;
var right:Number = scrollTrack.x;
var bottom:Number = scrollTrack.height - scrollFaceHeight + scrollTrack.y;
var dy:Number = 10;
var speed:Number = 10;
var moveVal:Number = (contentHeight - maskHeight) / (scrollHeight - scrollFaceHeight);

var faceDragging:Boolean = false;

scrollFace.addEventListener(MouseEvent.MOUSE_DOWN, face_Down);
function face_Down(e:MouseEvent):void {
	scrollFace.gotoAndPlay(2);
	
	var currPos:Number = scrollFace.y;
	scrollFace.startDrag(false, new Rectangle(left, top, 0, bottom - 20));
	faceDragging = true;
	
	stage.addEventListener(MouseEvent.MOUSE_MOVE, mse_move);
}

function mse_move(e:MouseEvent):void {
	dy = Math.abs(initPosition - scrollFace.y);
	contentMain.y = Math.round(dy * -1 * moveVal + initContentPos);
}

stage.addEventListener(MouseEvent.MOUSE_UP, face_Up);
function face_Up(e:MouseEvent):void {
	if (faceDragging) {
		scrollFace.gotoAndPlay(1);
		scrollFace.stopDrag();
		faceDragging = false;
	}
}

btnUp.addEventListener(MouseEvent.MOUSE_DOWN, btnUp_Down);
function btnUp_Down(e:MouseEvent):void {
	if (btnUp.enabled == true) {
		btnUp.addEventListener(Event.ENTER_FRAME, btnUp_Ent);
	}
}

function btnUp_Ent(e:Event):void {
	if (contentMain.y + speed < maskedView.y) {
		if (scrollFace.y <= top) {
			scrollFace.y = top;
		} else {
			scrollFace.y -= speed/moveVal;
		}
		contentMain.y += speed;
	} else {
		scrollFace.y = top;
		contentMain.y = maskedView.y;
		btnUp.removeEventListener(Event.ENTER_FRAME, btnUp_Ent);
	}
}

btnUp.addEventListener(MouseEvent.MOUSE_UP, btnUp_Up);
function btnUp_Up(e:MouseEvent):void {
	btnUp.removeEventListener(Event.ENTER_FRAME, btnUp_Ent);
}

btnUp.addEventListener(MouseEvent.ROLL_OUT, btnUp_Out);
function btnUp_Out(e:MouseEvent):void {
	btnUp.removeEventListener(Event.ENTER_FRAME, btnUp_Ent);
}

btnDown.addEventListener(MouseEvent.MOUSE_DOWN, btnDown_Down);
function btnDown_Down(e:MouseEvent):void {
	if (btnDown.enabled == true) {
		btnDown.addEventListener(Event.ENTER_FRAME, btnDown_Ent);
	}
}

function btnDown_Ent(e:Event):void {
	if (contentMain.y - speed > finalContentPos) {
		if (scrollFace.y >= bottom) {
			scrollFace.y = bottom;
		} else {
			scrollFace.y += speed/moveVal;
		}
		contentMain.y -= speed;
	} else {
		scrollFace.y = bottom;
		contentMain.y = finalContentPos;
		btnDown.removeEventListener(Event.ENTER_FRAME, btnDown_Ent);
	}
}

btnDown.addEventListener(MouseEvent.MOUSE_UP, btnDown_Up);
function btnDown_Up(e:MouseEvent):void {
	btnDown.removeEventListener(Event.ENTER_FRAME, btnDown_Ent);
}

btnDown.addEventListener(MouseEvent.ROLL_OUT, btnDown_Out);
function btnDown_Out(e:MouseEvent):void {
	btnDown.removeEventListener(Event.ENTER_FRAME, btnDown_Ent);
}
	 
if (contentHeight<maskHeight) {
	scrollFace.visible = false;
	btnUp.enabled = false;
	btnDown.enabled = false;
} else {
	scrollFace.visible = true;
	btnUp.enabled = true;
	btnDown.enabled = true;
}
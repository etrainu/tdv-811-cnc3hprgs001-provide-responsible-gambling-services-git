﻿/***********************************************************************************************
 *
 *	File: Preloader.as
 *  Current Version: 1.00
 *	Auth Date: 22nd July 09
 *  Author: Kim Serventy
 *
 *  Description: Generic Preloader to preload all content in an swf
 *
 *  Usage: This preloader requires the Preloader MovieClip from Preloader.fla to be included in
 *			in your movie with the instace name Preloder. Requires the preloader to be on
 *			frame 1 with a stop() and this file included, and all content to begin from frame 2.
 *  		This will not preload external swfs unless included in the external swf.
 *
 ***********************************************************************************************/

/**
 *  Function: PL_UPDATE
 *  Usage: Used to update the progress bar, arrow, percentage and bytes loaded
 */
function PL_UPDATE (e:ProgressEvent):void {
	var percent:Number = Math.floor( (e.bytesLoaded*100)/e.bytesTotal ); 
	
	Preloader.loadingBar.scaleX = percent/100;
	
	Preloader.percentClip.percentDisplay.text = percent + "%";
	Preloader.percentClip.x = Preloader.loadingBar.x + Preloader.loadingBar.width;
	
	Preloader.bytesDisplay.text = "loaded " + e.bytesLoaded + " of " + e.bytesTotal + " bytes";
}

/**
 *  Function: PL_COMPLETE
 *  Usage: Used to remove the event listeners, hide the Preloader and play the movie (frame 2)
 */
function PL_COMPLETE (e:Event):void {
	Preloader.visible = false;
	
	gotoAndPlay(2);
		
	loaderInfo.removeEventListener(ProgressEvent.PROGRESS, PL_UPDATE);
	loaderInfo.removeEventListener(Event.COMPLETE, PL_COMPLETE);
}

// Assign the progress events to PL_UPDATE
loaderInfo.addEventListener(ProgressEvent.PROGRESS, PL_UPDATE);
// Assign the complete event to PL_COMPLETE
loaderInfo.addEventListener(Event.COMPLETE, PL_COMPLETE);
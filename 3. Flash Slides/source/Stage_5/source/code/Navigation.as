﻿/***********************************************************************************************
 *
 *	File: Navigation.as
 *  Current Version: 1.00
 *	Auth Date: 7th September 09
 *  Author: Kim Serventy
 *
 *  Description: Simple Navigation controls for self contained flash
 *
 *			This Version is a modified version of SimpleNav.as, purpose built for the 
 *			new Project Template.
 *
 *  Usage: This navigation control requires your flash to be set up so that each page is
 *  		labelled Page1 Page2 etc. It requires 2 buttons to be on the page with instance
 *  		names of but_navForward and but_navBack. Include this file on the first frame
 *  		both navigation buttons exist and specify the maxPages variable (eg. maxPages = 8)
 *
 ***********************************************************************************************/

// maxPages used to stop navigation when the last page is reached
var maxPages:int = 1;

// currentPage used to tell what page the user is currently viewing
var currentPage:int = 1;

var pgLoader:Loader = new Loader();
addChild(pgLoader);
pgLoader.x = 0;
pgLoader.y = 72;

var xmlLoader:URLLoader = new URLLoader();
var xmlData:XML = new XML();
 
xmlLoader.addEventListener(Event.COMPLETE, LoadXML);
xmlLoader.load(new URLRequest(courseFolder + "content/xml/Shell.xml"));

function LoadXML(e:Event):void {
	xmlData = new XML(e.target.data);
	GetHeading(xmlData);
}

function GetHeading(shellInput:XML):void {
	txtHeader.text = shellInput.HEADER.(@PG == String(currentPage));
}

/**
 *  Function: navForward
 *  Usage: Used to navigate the user forward through the pages
 */
function navForward(e:MouseEvent):void {
			checkButtonsVisibilty_forward()
	if (currentPage != maxPages) {
		currentPage++;
		updatePage();
	}
}

/**
 *  Function: navBack
 *  Usage: Used to navigate the user backwards through the pages
 */
function navBack(e:MouseEvent):void {

	checkButtonsVisibilty_back()
	if (currentPage != 1) {
		currentPage--;
		updatePage();

	}
}

function updatePage():void {
	pgLoader.unloadAndStop();
	
	if (currentPage < 10) {
		txtCurrentPage.text = String(currentPage);
		pgLoader.load(new URLRequest(courseFolder + "content/swf/Page0" + currentPage + ".swf"));
	} else {
		txtCurrentPage.text = String(currentPage);
		pgLoader.load(new URLRequest(courseFolder + "content/swf/Page" + currentPage + ".swf"));
	}
	
	GetHeading(xmlData);
	SoundMixer.stopAll();
}

// Assign the forward button click event to navForward
but_navForward.addEventListener(MouseEvent.CLICK, navForward);
// Assign the backwards button click event to navBack
but_navBack.addEventListener(MouseEvent.CLICK, navBack);

updatePage();
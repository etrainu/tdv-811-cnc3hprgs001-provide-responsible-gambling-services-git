﻿map.act.buttonMode = enabled;
map.act.useHandCursor = true;
map.act.addEventListener(MouseEvent.ROLL_OVER, act_over);
map.act.addEventListener(MouseEvent.ROLL_OUT, act_out);
map.act.addEventListener(MouseEvent.CLICK, act_click);

function act_over(e:MouseEvent):void {
	map.act.gotoAndPlay("over");
}

function act_out(e:MouseEvent):void {
	map.act.gotoAndPlay("out");
}

function act_click(e:MouseEvent):void {
	SoundMixer.stopAll();
	map.infoMC.gotoAndStop("act_over");
}


map.nsw.buttonMode = enabled;
map.nsw.useHandCursor = true;
map.nsw.addEventListener(MouseEvent.ROLL_OVER, nsw_over);
map.nsw.addEventListener(MouseEvent.ROLL_OUT, nsw_out);
map.nsw.addEventListener(MouseEvent.CLICK, nsw_click);

function nsw_over(e:MouseEvent):void {
	map.nsw.gotoAndPlay("over");
}

function nsw_out(e:MouseEvent):void {
	map.nsw.gotoAndPlay("out");
}

function nsw_click(e:MouseEvent):void {
	SoundMixer.stopAll();
	map.infoMC.gotoAndStop("nsw_over");
}


map.nt.buttonMode = enabled;
map.nt.useHandCursor = true;
map.nt.addEventListener(MouseEvent.ROLL_OVER, nt_over);
map.nt.addEventListener(MouseEvent.ROLL_OUT, nt_out);
map.nt.addEventListener(MouseEvent.CLICK, nt_click);

function nt_over(e:MouseEvent):void {
	map.nt.gotoAndPlay("over");
}

function nt_out(e:MouseEvent):void {
	map.nt.gotoAndPlay("out");
}

function nt_click(e:MouseEvent):void {
	SoundMixer.stopAll();
	map.infoMC.gotoAndStop("nt_over");
}


map.qld.buttonMode = enabled;
map.qld.useHandCursor = true;
map.qld.addEventListener(MouseEvent.ROLL_OVER, qld_over);
map.qld.addEventListener(MouseEvent.ROLL_OUT, qld_out);
map.qld.addEventListener(MouseEvent.CLICK, qld_click);

function qld_over(e:MouseEvent):void {
	map.qld.gotoAndPlay("over");
}

function qld_out(e:MouseEvent):void {
	map.qld.gotoAndPlay("out");
}

function qld_click(e:MouseEvent):void {
	SoundMixer.stopAll();
	map.infoMC.gotoAndStop("qld_over");
}


map.sa.buttonMode = enabled;
map.sa.useHandCursor = true;
map.sa.addEventListener(MouseEvent.ROLL_OVER, sa_over);
map.sa.addEventListener(MouseEvent.ROLL_OUT, sa_out);
map.sa.addEventListener(MouseEvent.CLICK, sa_click);

function sa_over(e:MouseEvent):void {
	map.sa.gotoAndPlay("over");
}

function sa_out(e:MouseEvent):void {
	map.sa.gotoAndPlay("out");
}

function sa_click(e:MouseEvent):void {
	SoundMixer.stopAll();
	map.infoMC.gotoAndStop("sa_over");
}


map.tas.buttonMode = enabled;
map.tas.useHandCursor = true;
map.tas.addEventListener(MouseEvent.ROLL_OVER, tas_over);
map.tas.addEventListener(MouseEvent.ROLL_OUT, tas_out);
map.tas.addEventListener(MouseEvent.CLICK, tas_click);

function tas_over(e:MouseEvent):void {
	map.tas.gotoAndPlay("over");
}

function tas_out(e:MouseEvent):void {
	map.tas.gotoAndPlay("out");
}

function tas_click(e:MouseEvent):void {
	SoundMixer.stopAll();
	map.infoMC.gotoAndStop("tas_over");
}


map.vic.buttonMode = enabled;
map.vic.useHandCursor = true;
map.vic.addEventListener(MouseEvent.ROLL_OVER, vic_over);
map.vic.addEventListener(MouseEvent.ROLL_OUT, vic_out);
map.vic.addEventListener(MouseEvent.CLICK, vic_click);

function vic_over(e:MouseEvent):void {
	map.vic.gotoAndPlay("over");
}

function vic_out(e:MouseEvent):void {
	map.vic.gotoAndPlay("out");
}

function vic_click(e:MouseEvent):void {
	SoundMixer.stopAll();
	map.infoMC.gotoAndStop("vic_over");
}


map.wa.buttonMode = enabled;
map.wa.useHandCursor = true;
map.wa.addEventListener(MouseEvent.ROLL_OVER, wa_over);
map.wa.addEventListener(MouseEvent.ROLL_OUT, wa_out);
map.wa.addEventListener(MouseEvent.CLICK, wa_click);

function wa_over(e:MouseEvent):void {
		map.wa.gotoAndPlay("over");
}

function wa_out(e:MouseEvent):void {
	map.wa.gotoAndPlay("out");
}

function wa_click(e:MouseEvent):void {
	SoundMixer.stopAll();
	map.infoMC.gotoAndStop("wa_over");
}
